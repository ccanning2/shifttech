'use strict'

var application = angular.module('demo', ['ui.bootstrap', 'app.controllers', 'app.services']);
application.constant("CONSTANTS", {
    getUserByIdUrl : "/user/getUser/",
    getAllUsers : "/user/getAllUsers",
    saveUser : "/user/saveUser"
});