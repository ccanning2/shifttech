package com.chris.canning.shifttech.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.chris.canning.shifttech.dto.BinListCreditCard;
import com.chris.canning.shifttech.dto.CreditCardDto;
import com.chris.canning.shifttech.dto.CreditCardValidation;
import com.chris.canning.shifttech.service.CreditCardService;
import com.chris.canning.shifttech.utils.Constants;

@RequestMapping("/creditCards")
@RestController
public class CreditCardController {

	@Autowired
	private CreditCardService creditCardService;

	@RequestMapping(Constants.GET_ALL_CREDIT_CARDS)
	public List<CreditCardDto> getAllCreditCards() {
		return creditCardService.getAllCreditCards();
	}

	@PostMapping
	@RequestMapping(value = Constants.STORE_CREDIT_CARD)
	public ResponseEntity<CreditCardValidation> storeCreditCard(@RequestBody CreditCardDto creditCard) {
		boolean valid = this.creditCardService.validateCreditCardNumber(creditCard.getNumber());

		CreditCardValidation creditCardValidation = new CreditCardValidation();
		creditCardValidation.setValid(valid);

		if (!valid) {
			return ResponseEntity.ok().body(creditCardValidation);
		}

		String firstEightDigits = creditCard.getNumber().substring(0, 7);

		List<String> bannedCountries = new ArrayList<>(Arrays.asList("IT", "US", "BR", "ES"));

		// Banned:
		// Brazil BR: 4195981865979205
		// Italy IT: 4741800846642758
		// USA US: 4148012772280442
		// Spain ES: 4836630497565115

		// Unbanned:
		// El Salvador SV : 4508209221327253
		// India IN : 4129055388989164
		// France FR : 4977431395316787

		// IMPORTANT
		// Using https://binlist.net/ to get card details.
		// Requests are throttled at 10 per minute with a burst allowance of 10.
		// If you hit the speed limit the service will return a 429 http status code.

		RestTemplate restTemplate = new RestTemplate();
		BinListCreditCard binListCreditCard = restTemplate.getForObject(Constants.BIN_LIST_BASE_URL + firstEightDigits,
				BinListCreditCard.class);

		Boolean isBanned = bannedCountries.stream()
				.filter(country -> country.equalsIgnoreCase(binListCreditCard.getCountry().getAlpha2())).findAny()
				.isPresent();

		creditCardValidation.setBanned(isBanned);

		if (!isBanned && valid) {
			this.creditCardService.saveCreditCard(creditCard);
		}

		return ResponseEntity.ok().body(creditCardValidation);
	}
}
