package com.chris.canning.shifttech.converter;

import com.chris.canning.shifttech.dto.CreditCardDto;
import com.chris.canning.shifttech.entity.CreditCard;

public class CreditCardConverter {

	public static CreditCard dtoToEntity(CreditCardDto creditCardDto) {
		CreditCard creditCard = new CreditCard(creditCardDto.getName());
		creditCard.setId(creditCardDto.getId());
		creditCard.setNumber(creditCardDto.getNumber());
		return creditCard;
	}

	public static CreditCardDto entityToDto(CreditCard creditCard) {
		return new CreditCardDto(creditCard.getId(), creditCard.getName(), creditCard.getNumber());
	}
}
