package com.chris.canning.shifttech.dto;

public class BinListCountry {

	private Integer numeric;
	private String alpha2;
	private String name;
	private String emoji;
	private String currency;
	private Long latitude;
	private Long longitude;

	public Integer getNumeric() {
		return numeric;
	}

	public void setNumeric(Integer numeric) {
		this.numeric = numeric;
	}

	public String getAlpha2() {
		return alpha2;
	}

	public void setAlpha2(String alpha2) {
		this.alpha2 = alpha2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmoji() {
		return emoji;
	}

	public void setEmoji(String emoji) {
		this.emoji = emoji;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Long getLatitude() {
		return latitude;
	}

	public void setLatitude(Long latitude) {
		this.latitude = latitude;
	}

	public Long getLongitude() {
		return longitude;
	}

	public void setLongitude(Long longitude) {
		this.longitude = longitude;
	}
}
