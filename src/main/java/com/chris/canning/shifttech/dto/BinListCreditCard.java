package com.chris.canning.shifttech.dto;

public class BinListCreditCard {

	private BinListNumber number;
	private String scheme;
	private String type;
	private String brand;
	private Boolean prepaid;
	private BinListCountry country;
	private BinListBank bank;

	public BinListNumber getNumber() {
		return number;
	}

	public void setNumber(BinListNumber number) {
		this.number = number;
	}

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Boolean getPrepaid() {
		return prepaid;
	}

	public void setPrepaid(Boolean prepaid) {
		this.prepaid = prepaid;
	}

	public BinListCountry getCountry() {
		return country;
	}

	public void setCountry(BinListCountry country) {
		this.country = country;
	}

	public BinListBank getBank() {
		return bank;
	}

	public void setBank(BinListBank bank) {
		this.bank = bank;
	}
}
