package com.chris.canning.shifttech.dto;

public class CreditCardValidation {

	private Boolean valid;
	private Boolean banned;

	public CreditCardValidation() {

	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	public Boolean getBanned() {
		return banned;
	}

	public void setBanned(Boolean banned) {
		this.banned = banned;
	}
}
