package com.chris.canning.shifttech.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.chris.canning.shifttech.entity.CreditCard;

@Repository
public interface CreditCardRepository extends JpaRepository<CreditCard, Integer> {

	@Query("SELECT cc FROM CreditCard cc WHERE cc.number = :number")
	public CreditCard findCreditCardByNumber(@Param("number") String number);

}
