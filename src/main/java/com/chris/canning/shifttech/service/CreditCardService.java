package com.chris.canning.shifttech.service;

import java.util.List;

import com.chris.canning.shifttech.dto.CreditCardDto;

public interface CreditCardService {

	public CreditCardDto getCreditCardByNumber(String creditCardNumber);

	public void saveCreditCard(CreditCardDto creditCardDto);

	public List<CreditCardDto> getAllCreditCards();

	boolean validateCreditCardNumber(String creditCardNumber);
}
