package com.chris.canning.shifttech.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chris.canning.shifttech.converter.CreditCardConverter;
import com.chris.canning.shifttech.dto.CreditCardDto;
import com.chris.canning.shifttech.repository.CreditCardRepository;
import com.chris.canning.shifttech.service.CreditCardService;

@Service
public class CreditCardServiceimpl implements CreditCardService {

	@Autowired
	CreditCardRepository creditCardRepository;

	@Override
	public CreditCardDto getCreditCardByNumber(String creditCardNumber) {
		return CreditCardConverter.entityToDto(creditCardRepository.findCreditCardByNumber(creditCardNumber));
	}

	@Override
	public void saveCreditCard(CreditCardDto creditCardDto) {
		if (this.creditCardRepository.findCreditCardByNumber(creditCardDto.getNumber()) == null) {
			creditCardRepository.save(CreditCardConverter.dtoToEntity(creditCardDto));
		}
	}

	@Override
	public List<CreditCardDto> getAllCreditCards() {
		return creditCardRepository.findAll().stream().map(CreditCardConverter::entityToDto)
				.collect(Collectors.toList());
	}

	@Override
	public boolean validateCreditCardNumber(String creditCardNumber) {
		if (!creditCardNumber.matches("[0-9]+")) {
			return false;
		}

		int[] ints = new int[creditCardNumber.length()];

		for (int i = 0; i < creditCardNumber.length(); i++) {
			ints[i] = Integer.parseInt(creditCardNumber.substring(i, i + 1));
		}

		for (int i = ints.length - 2; i >= 0; i = i - 2) {
			int j = ints[i];
			j = j * 2;

			if (j > 9) {
				j = j % 10 + 1;
			}

			ints[i] = j;
		}

		int sum = 0;

		for (int i = 0; i < ints.length; i++) {
			sum += ints[i];
		}

		return sum % 10 == 0;
	}
}
