package com.chris.canning.shifttech.utils;

public interface Constants {

	// API Endpoints
	static final String GET_ALL_CREDIT_CARDS = "/creditCard/all";
	static final String STORE_CREDIT_CARD = "/creditCard/store";

	// Bin List API Base URL
	static final String BIN_LIST_BASE_URL = "https://lookup.binlist.net/";
}
