'use strict'

var module = angular.module('app.controllers', []);

module
		.controller(
				"CreditCardController",
				[
						"$scope",
						"CreditCardService",
						function($scope, CreditCardService) {
							$scope.creditCard = {
								id : null,
								number : null,
								valid : null,
								banned : null
							};

							$scope.creditCards = [];

							CreditCardService.getAllCreditCards().then(
									function(result) {
										console.log('Credit Cards: ',
												result.data);
										$scope.creditCards = result.data;
									});

							$scope.storeCreditCard = function() {
								console.log('Validating credit card: ',
										$scope.creditCard);

								CreditCardService
										.storeCreditCard($scope.creditCard)
										.then(
												function(result) {
													if (result && result.data) {
														console
																.log(
																		"Credit card is valid: ",
																		result.data.valid,
																		' | is banned: ',
																		result.data.banned);
														$scope.creditCard.valid = result.data.valid;
														$scope.creditCard.banned = result.data.banned;

														CreditCardService
																.getAllCreditCards()
																.then(
																		function(
																				result) {
																			console
																					.log(
																							'Credit Cards: ',
																							result.data);
																			$scope.creditCards = result.data;
																		});
													} else {
														console
																.log(
																		"Credit card is not valid: ",
																		result.data);
														$scope.creditCard.valid = false;
													}
												},
												function(reason) {
													console
															.log(
																	"Error occurred while validating credit card: ",
																	reason);
												},
												function(value) {
													console
															.log(
																	"Error occurred while validating credit card: ",
																	value);
												});
							}
						} ]);