'use strict'

var application = angular.module('shifttech', [ 'ui.bootstrap',
		'app.controllers', 'app.services' ]);

application.constant("CONSTANTS", {
	getAllCreditCards : "/creditCards/creditCard/all",
	storeCreditCard : "/creditCards/creditCard/store"
});