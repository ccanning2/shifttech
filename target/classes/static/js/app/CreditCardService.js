'use strict'

angular.module('app.services', []).factory('CreditCardService',
		[ "$http", "CONSTANTS", function($http, CONSTANTS) {
			var service = {};

			service.getAllCreditCards = function() {
				return $http.get(CONSTANTS.getAllCreditCards);
			}

			service.storeCreditCard = function(creditCard) {
				return $http.post(CONSTANTS.storeCreditCard, creditCard);
			}

			return service;
		} ]);